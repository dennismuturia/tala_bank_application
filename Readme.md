# Tala Bank Application Test
This is a bank application written in spring-boot.

## Requirements
1. Java 8
2. Maven 3.6.3
3. Postgres 12

## Instructions to run the applications
Once cloned the application, open the terminal on the root folder of the application build
a package for the application.

```
mvn package
```

In case you would like to run tests run:

```
mvn test
```
Code coverage documents will be generated if you would like to
view the code coverage

```
/bank_dumo/target/site/jacoco/
```

A jar file will be generated at 

```
/bank_demo/target/bank_demo-0.0.1-SNAPSHOT.jar
```

Create an application.properties file and add the database information like this:
```
spring.jpa.database=POSTGRESQL
spring.datasource.platform=postgres
spring.datasource.url=jdbc:postgresql://localhost:5432/<preffered-database-name>
spring.datasource.username=<your-poatgres-username>
spring.datasource.password=<your-postgres-password>
spring.jpa.show-sql=false
spring.datasource.hikari.connection-timeout=5000
spring.datasource.hikari.maximum-pool-size=30
spring.datasource.hikari.minimum-idle=10
spring.jpa.generate-ddl=true
```
Enter your database username, password and also preffered-database-name

Once the jar file is generated. Copy the jar file to any folder of your choice in your PC
and execute the jar file like this:

```
java -jar bank_demo-0.0.1-SNAPSHOT.jar --spring.config.location=/<your application.properties-location>/application.properties
```



No need to worry about deploying the jar file in a Tomcat folder since
Tomcat is already incorporated in the application.

NB:Before running your application make sure you have created a db and entered the db name here:
```
spring.datasource.url=jdbc:postgresql://localhost:5432/<preffered-database-name>
```

## Instruction on carrying out transactions in the application
There are 3 endpoints the application
1. Balance
2. Deposit 
3. Withdraw

NB: A user is already incorporated into the application. The user id is 1. The user has been initialized with 100 balance
The port for the application is 8080
### Balance
To get the balance use GET request on this endpoint
```
/bank/balance/user/{id}
```
Rememeber, replace {id} with 1.
A response will given back. If the id is not 1, the a HTTP status code will be a 404 and 
the response will be: User not found. Else a status code of 200 and a response of the user balance 
will be provided.

### Deposit
To add a deposit, use a POST request on this endpoint
```
/bank/deposit/user/{id}/{amount}
```
NB: Replace the {amount} with an int amount.
If the amount is successfully deposited then this will be the response with HTTP status of 200

```
Deposit of {amount} has been accepted
```

if the amount of the transaction is above what is required, then the response will be:
```
Maximum amount being deposited for this transaction exceeded
```
with a status code of 406

else if the total amount of deposits is > 150000 on the specific day then the response will be:
```
Maximum total amount for the day has been reached.
```

if the frequency of deposit transactons > 4 the response is:
```
Maximum number of frequency reached
```
with a status code of 406
### Withdraw
To withdraw an  amount, use a POST request on this endpoint
```
/bank/withdraw/user/{id}/{amount}
```

if the frequency of withdrawals > 3, the respense will be:
```
Maximum frequency withdrawal for the day has been acheived
```
with a status code of 406

If the user tries to withdraw an amount higher than that is in his account, The response is:
```
Not enough money to withdraw
```
with a 406 status code

if the total withdrawal amounts for the day > 50000, the response is:
```
Maximum total withdrawal amount for the day has been reached.
```
with a 406 status code

If the transaction withdrawal amount > 20000, the response will be :
```
Maximum amount being withdrawn for this transaction exceeded
```
with a 406 status code

if there is a successful withdrawal the response will be:
```
Withdrawal of {amount} has been accepted"
```
