package com.tala.bank_demo;

import com.tala.bank_demo.Model.TransactionTypes;
import com.tala.bank_demo.Model.Transactions;
import com.tala.bank_demo.Model.Users;
import com.tala.bank_demo.Repository.TransactionRepository;
import com.tala.bank_demo.Repository.UserRepository;
import com.tala.bank_demo.Service.TransactionService;
import com.tala.bank_demo.Service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@AutoConfigureMockMvc
class BankDemoApplicationTests {

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    UserRepository userRepository;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    TransactionService transactionService;

    @Autowired
    UserService userService;

    @Test
    public void getBalanceAfterTransaction(){
        Users theUser = new Users("dmuturia", "dennis", "muturia", 100);
        Mockito.when(userRepository.save(theUser)).thenReturn(theUser);
        transactionService.depositAmount(200, theUser);
        transactionService.depositAmount(400, theUser);
        transactionService.withdrawAmount(200, theUser);
        theUser.setBalance(200 +400 -100);
        assertEquals(500, theUser.getBalance());

    }

    @Test
    public void exceededAmountDepositPerTransaction(){
        assertEquals("Maximum amount being deposited for this transaction exceeded", transactionService.depositAmount(400001, new Users("dmuturia", "dennis", "muturia", 100)));
    }

    @Test
    public void verifyTheUserSizeToOne(){
        Mockito.when(userRepository.findAll())
                .thenReturn(Stream.of(new Users("dmuturia", "dennis", "muturia", 100))
                        .collect(Collectors.toList()));
        assertEquals(1, userRepository.findAll().size());
    }

    @Test
    public void depositMaxVerification(){
        int amount = 40001;
        Users theUser = new Users("dmuturia", "dennis", "muturia", 100);
        transactionService.depositAmount(amount, theUser);

        assertEquals("Maximum amount being deposited for this transaction exceeded", transactionService.depositAmount(amount, theUser));
    }

    @Test
    public void depositMaxFrequency(){
        Mockito.when(transactionRepository.getCountTransactions(TransactionTypes.DEPOSIT.name()))
                .thenReturn(Stream.of(new Transactions(System.currentTimeMillis(), new Date(), 3000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.DEPOSIT.name()), new Transactions(System.currentTimeMillis(), new Date(), 3000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.DEPOSIT.name()), new Transactions(System.currentTimeMillis(), new Date(), 3000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.DEPOSIT.name()), new Transactions(System.currentTimeMillis(), new Date(), 3000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.DEPOSIT.name()), new Transactions(System.currentTimeMillis(), new Date(), 3000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.DEPOSIT.name())).collect(Collectors.toList()));
        for (int i =0; i<=transactionRepository.getCountTransactions(TransactionTypes.DEPOSIT.name()).size(); i++) {
            transactionService.depositAmount(3000, new Users("dmuturia", "dennis", "muturia", 100));
        }
        transactionRepository.deleteAll();
        if(transactionRepository.getCountTransactions(TransactionTypes.DEPOSIT.name()).size() > 4){
            assertEquals("Maximum number of frequency reached", transactionService.depositAmount(100, new Users("dmuturia", "dennis", "muturia", 100)));
        }


    }





    @Test
    public void exceedMaximumFrequencyWithdrawalPerTransaction(){
        assertEquals("Maximum amount being withdrawn for this transaction exceeded", transactionService.withdrawAmount(30000, new Users("dmuturia", "dennis", "muturia", 40000)));
    }

    @Test
    public void exceedMaximumAmountWithdrawnForTheDay(){
        Mockito.when(transactionRepository.getCountTransactions(TransactionTypes.WITHDRAW.name()))
                .thenReturn(Stream.of(new Transactions(System.currentTimeMillis(), new Date(), 3000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.WITHDRAW.name()), new Transactions(System.currentTimeMillis(), new Date(), 19000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.WITHDRAW.name()), new Transactions(System.currentTimeMillis(), new Date(), 19000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.WITHDRAW.name()), new Transactions(System.currentTimeMillis(), new Date(), 19000,
                        new Users("dmuturia", "dennis", "muturia", 100),
                        TransactionTypes.WITHDRAW.name())).collect(Collectors.toList()));
        for (int i = 0; i <= transactionRepository.getCountTransactions(TransactionTypes.WITHDRAW.name()).size() ; i++) {
            transactionService.withdrawAmount(19999, new Users("dmuturia", "dennis", "muturia", 100000));
        }

        assertEquals("Maximum total withdrawal amount for the day has been reached.", transactionService.withdrawAmount(19999, new Users("dmuturia", "dennis", "muturia", 100000)));

    }

    @Test
    public void  notEnoughAmountToWithdraw(){
        assertEquals("Not enough money to withdraw", transactionService.withdrawAmount(19999, new Users("dmuturia", "dennis", "muturia", 10000)));

    }

}
