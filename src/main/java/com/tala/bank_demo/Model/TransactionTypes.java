package com.tala.bank_demo.Model;

public enum TransactionTypes {
    DEPOSIT,
    WITHDRAW
}
