package com.tala.bank_demo.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
@Data
public class Transactions {
    @Id
    private long transactionId;
    private Date transactionDate;
    private int amount;
    @OneToOne
    private Users users;
    private String transactionType;

    public Transactions(){}

    public Transactions(long transactionId, Date transactionDate, int amount, Users users, String transactionType){
        this.transactionId = transactionId;
        this.transactionDate = transactionDate;
        this.amount = amount;
        this.users = users;
        this.transactionType = transactionType;
    }
}
