package com.tala.bank_demo.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userId;//For simplicity I will use auto generation of ids
    private String username;
    private String firstName;
    private String lastName;
    private int balance;

    public Users(){}

    public Users( String username, String firstName, String lastName, int balance){
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
    }
}
