package com.tala.bank_demo;

import com.tala.bank_demo.Model.Users;
import com.tala.bank_demo.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Init implements CommandLineRunner {
    @Autowired
    private UserRepository userRepository;
    @Override
    public void run(String... args) throws Exception {
        userRepository.save(new Users("dmuturia", "dennis", "muturia", 100));
    }
}
