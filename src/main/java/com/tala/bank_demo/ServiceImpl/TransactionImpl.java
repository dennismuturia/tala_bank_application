package com.tala.bank_demo.ServiceImpl;

import com.tala.bank_demo.Model.Users;

public interface TransactionImpl {
    public String depositAmount(int amount, Users user);
    public String withdrawAmount(int amount, Users user);
}
