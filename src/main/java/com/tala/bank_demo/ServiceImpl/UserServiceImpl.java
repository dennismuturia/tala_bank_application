package com.tala.bank_demo.ServiceImpl;

import com.tala.bank_demo.Model.Users;
import org.json.JSONException;

public interface UserServiceImpl {
    int getUserBalance(long id) throws JSONException;
    Users userGetOne(long id);
}
