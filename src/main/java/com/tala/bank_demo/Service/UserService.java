package com.tala.bank_demo.Service;

import com.tala.bank_demo.Model.Users;
import com.tala.bank_demo.Repository.UserRepository;
import com.tala.bank_demo.ServiceImpl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserServiceImpl {
    @Autowired
    private UserRepository userRepository;

    @Override
    public int getUserBalance(long id){
        return userRepository.getOne(id).getBalance(); //Returns the whole users model. THe balance is bundled there
    }

    @Override
    public Users userGetOne(long id) {
        return userRepository.getOne(id);
    }
}
