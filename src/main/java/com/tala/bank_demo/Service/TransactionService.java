package com.tala.bank_demo.Service;

import com.tala.bank_demo.Model.TransactionTypes;
import com.tala.bank_demo.Model.Transactions;
import com.tala.bank_demo.Model.Users;
import com.tala.bank_demo.Repository.TransactionRepository;
import com.tala.bank_demo.Repository.UserRepository;
import com.tala.bank_demo.ServiceImpl.TransactionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TransactionService implements TransactionImpl {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private UserRepository userRepository;


    @Override
    public String depositAmount(int amount, Users user) {
        int totalDepositAmount =0;
        int totalDepositTransaction =0;
        String returnStatements;
        if(amount > 40000){
            return  "Maximum amount being deposited for this transaction exceeded";

        }
        List<Transactions> depositTransactions = transactionRepository.getCountTransactions(TransactionTypes.DEPOSIT.name());
        for (Transactions trans:depositTransactions) {
            totalDepositAmount += trans.getAmount();
            totalDepositTransaction +=1;
        }
        totalDepositAmount = totalDepositAmount + amount;

        if(totalDepositAmount > 150000){
            return "Maximum total amount for the day has been reached.";

        }
        if(totalDepositTransaction<=3){
            Transactions transactions = new Transactions();
            transactions.setTransactionId(System.currentTimeMillis());
            transactions.setTransactionType(TransactionTypes.DEPOSIT.name());
            transactions.setAmount(amount);
            transactions.setTransactionDate(new Date());
            transactions.setUsers(user);

            if(totalDepositTransaction == 0){
                user.setBalance(totalDepositAmount + user.getBalance());
            }else{
                user.setBalance(amount + user.getBalance());
            }



            userRepository.save(user);
            transactionRepository.save(transactions);
            return  "Deposit of "+ amount+" has been accepted";
        }else{
            returnStatements = "Maximum number of frequency reached";
        }

        if(returnStatements.equals("")){
            returnStatements = "Amount not saved";
        }

        return returnStatements;
    }

    @Override
    public String withdrawAmount(int amount, Users user) {
        AtomicInteger totalWithdrawAmountTransaction = new AtomicInteger();
        AtomicInteger totalWithdrawFrequency = new AtomicInteger();


        String returnStatements = "";
        if(amount > user.getBalance()){
            return "Not enough money to withdraw";
        }else{
            if(amount > 20000)
                return "Maximum amount being withdrawn for this transaction exceeded";

            List<Transactions> depositTransactions = transactionRepository.getCountTransactions(TransactionTypes.WITHDRAW.name());
            depositTransactions.forEach(x ->{
                totalWithdrawAmountTransaction.addAndGet(x.getAmount());
                totalWithdrawFrequency.addAndGet(1);
            });

            if(totalWithdrawAmountTransaction.get() > 50000){
                returnStatements = "Maximum total withdrawal amount for the day has been reached.";
                return returnStatements;
            }

            if(totalWithdrawFrequency.get() < 3 ){
                Transactions transactions = new Transactions();
                transactions.setTransactionId(System.currentTimeMillis());
                transactions.setTransactionType(TransactionTypes.WITHDRAW.name());
                transactions.setAmount(amount);
                transactions.setTransactionDate(new Date());
                transactions.setUsers(user);

                user.setBalance(user.getBalance() - amount);


                userRepository.save(user);
                transactionRepository.save(transactions);
                returnStatements = "Withdrawal of "+ amount+" has been accepted";
            }else{
                returnStatements = "Maximum frequency withdrawal for the day has been acheived";
            }
        }
        return returnStatements;
    }


}
