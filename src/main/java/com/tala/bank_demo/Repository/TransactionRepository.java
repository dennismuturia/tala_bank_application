package com.tala.bank_demo.Repository;

import com.tala.bank_demo.Model.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transactions, Long> {
    @Query(nativeQuery = true,
    value = "select * from transactions where cast(transaction_date as date)=CURRENT_DATE and transaction_type=:valls")
    List<Transactions>getCountTransactions(@Param("valls")String transactionType);
}
