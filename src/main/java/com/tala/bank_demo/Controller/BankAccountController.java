package com.tala.bank_demo.Controller;

import com.tala.bank_demo.Model.Users;
import com.tala.bank_demo.Service.TransactionService;
import com.tala.bank_demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bank")
public class BankAccountController {
    @Autowired
    private UserService userService;

    @Autowired
    private TransactionService transactionService;

    @GetMapping("/balance/user/{id}")
    public ResponseEntity<?> getUserBalance(@PathVariable("id")long id) {
        if(id!=1){
            return new ResponseEntity<>("User Not found", HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity<>(userService.getUserBalance(id), HttpStatus.OK);
        }

    }

    @PostMapping("/deposit/user/{id}/{amount}")
    public ResponseEntity<?> depositAmount(@PathVariable("id")long id, @PathVariable("amount")int amount) {
        Users user = userService.userGetOne(id);
        String response = transactionService.depositAmount(amount, user);

        if(response.equals("Deposit of "+ amount+" has been accepted")){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @PostMapping("/withdraw/user/{id}/{amount}")
    public ResponseEntity<?> withdrawAmount(@PathVariable("id")long id, @PathVariable("amount")int amount){
        Users user = userService.userGetOne(id);
        String response = transactionService.withdrawAmount(amount, user);

        if(response.equals("Withdrawal of "+ amount+" has been accepted")){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);

        }
    }
}
